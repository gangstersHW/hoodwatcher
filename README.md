# README #

Esse sistema foi desenvolvido para ajudar nossos usuários a acompanhar seus artistas de hip hop favoritos.

### O repositorio contem

- Versão inicial do programa
- Documentação do sistema


### Como configurar seu ambiente de desenvolvimento

git clone //endereço que o bitbucket informa para clonar
//para baixar todo o projeto
//ao baixar o repositório "hoodwatcher", no esclipse informar que o WorkSpace é o diretório "nome do projeto"
//executar o git bash com os "git status", "git add" e etc dentro do repositório "hoodwatcher"
git status //para ver se algo está diferente
git add --all //para atualizar os arquivos que estão local
git commit -m 'texto' //para incluir o comentario
git push origin master //para enviar o novo projeto

O sistema necessita dos seguintes componentes

- EJB server
- Maven
- Primeface

Nossos testes são desenvolvidos em JUnit.
Ainda não temos uma versão para deploy.


### Como contribuir

Faça um fork da versão atual do repositório e participe
- Escrevendo testes
- Code review


### Com quem falar?

Felipe Guerreiro - 
Diego Avalos - 
Tarcisio Monteiro