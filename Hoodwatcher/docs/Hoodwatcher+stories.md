Hist�ria 1

SENDO um f� de m�sicas de rap
POSSO saber se um novo �lbum foi lan�ado
PARA poder atualizar minha biblioteca de m�sicas.

----

Cen�rio: Novo �lbum lan�ado

Dado que o �lbum "Swish" foi lan�ado
E "Felipe" est� logado no sistema
Quando acessar o website
Ent�o o sistema deve mostrar em uma linha de tempo lan�amentos da �ltima semana
E permitir visualizar informa��es sobre o �lbum "Swish".

----
********
----

Hist�ria 2

SENDO um f� de m�sicas de rap
POSSO saber se um novo single foi lan�ado
PARA poder atualizar minha biblioteca de m�sicas.

----

Cen�rio: Novo single lan�ado

Dado que um single "Bitch don't kill my vibe" foi lan�ado
E "Felipe" est� logado no sistema
Quando acessar o website
Ent�o o sistema deve mostrar em uma linha de tempo lan�amentos da �ltima semana
E permitir visualizar informa��es sobre o single "Bitch don't kill my vibe".


----
********
----

Hist�ria 3

Adicionar Artistas

SENDO um f� de m�sicas de rap e hip hop
POSSO adicionar artistas a minha timeline
PARA poder atualizar minha biblioteca.

----

Cen�rio: Adicionando artista a biblioteca

Dado que o usuario "Felipe" est� logado
e que ele quer adicionar o artista "Kane West"
quando ele inserir o nome desse artista
ent�o o sistema deve associar esse artista a sua biblioteca.

----

Cen�rio: Artista j� existente na bilbioteca

Dado que o usuario "Felipe" est� logado
e que ele quer adicionar o artista "Kane West"
quando ele inserir o nome desse artista
E esse artista j� existir em sua biblioteca
ent�o o sistema deve avisa- lo que o artista j� se encontra adicionado

----
********
----

Hist�ria 4

Adicionar Albuns

SENDO um f� de m�sicas de rap e hip hop
POSSO adicionar Albuns a minha timeline
PARA poder atualizar minha biblioteca.

----

Cen�rio: Adicionando album novo a biblioteca

Dado que o usuario "Felipe" est� logado
e que ele quer adicionar o album "School Dropout"
quando ele inserir o nome desse Album
ent�o o sistema deve associar esse Album a sua biblioteca.

----

Cen�rio: Album j� existente na bilbioteca

Dado que o usuario "Felipe" est� logado
e que ele quer adicionar o album "School Dropout"
quando ele inserir o nome desse Album
E esse Album j� existir em sua biblioteca
ent�o o sistema deve avisa- lo que o Album j� se encontra adicionado 

----

Cen�rio: Adicionando um novo single a biblioteca

Dado que o usuario "Felipe" est� logado
e que ele quer adicionar o Single "Bitch don't kill my vibe"
quando ele inserir o nome desse Single
E esse Album j� existir em sua biblioteca
ent�o o sistema deve associar esse Single a sua biblioteca.

----

Historia 5

SENDO um f� de m�sicas de rap e hip hop
POSSO remover artistas da minha lista de Prefer�ncias
PARA poder ter uma lista atualizada.

----

Cen�rio: Removendo uma single da minha lista de Prefer�ncias

Dado que o usuario "Felipe" est� logado
e que ele quer remover o Single "Bitch don't kill my vibe"
quando ele inserir o nome desse Single
E esse Album j� existir em sua biblioteca
ent�o o sistema deve desassociar esse Single da sua biblioteca.

----

Cen�rio: Removendo uma Album da minha lista de Prefer�ncias

Dado que o usuario "Felipe" est� logado
e que ele quer remover o Album "School Dropout"
quando ele inserir o nome desse Album
E esse Album j� existir em sua biblioteca
ent�o o sistema deve desassociar esse Album da sua biblioteca.

----

Cenario: Removendo um artista da minha lista de Prefer�ncias - 11/06/2015

Dado que o usuario "Felipe" est� logado
E que ele quer remover o artista "Kanye West"
quando ele inserir o nome desse Artista
E esse artista j� existir em sua biblioteca
ent�o o sistema deve desassociar esse album da sua biblioteca

----

Historia 6

Cenario: Mostrar sugest�o baseado nas preferencias

Dado que o usuario "Felipe" est� logado
E que ele j� adicionou "Kanye West" a sua lista de preferencias
quando ele acessar sua timeline
ent�o o sistema deve sugerir que ele ou�a o artista Drake



