package com.hoodwatcher.rn;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hoodwatcher.db.BibliotecaMusicaDB;
import com.hoodwatcher.ed.BibliotecaMusica;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Usuario;

@Stateless
public class BibliotecaMusicaRN extends AbstractRN<BibliotecaMusica, Long> {

  private BibliotecaMusicaDB bibliotecaMusicaDB;

  @PersistenceContext(unitName = "HoodWatcherUnit")
  private EntityManager em;

  public BibliotecaMusicaRN() {
	super(BibliotecaMusica.class);
  }

  @Override
  protected EntityManager getEntityManager() {
	return em;
  }

  @Override
  @PostConstruct
  public void init() {
	bibliotecaMusicaDB = new BibliotecaMusicaDB(getEntityManager());
  }

  public BibliotecaMusica salvarLike(BibliotecaMusica bibliotecaMusica) {
	deletarPreferencia(bibliotecaMusica.getUsuario(), bibliotecaMusica.getMusica());

	bibliotecaMusica.setGostei(1L);
	return bibliotecaMusicaDB.save(bibliotecaMusica);
  }

  public BibliotecaMusica salvarNoLike(BibliotecaMusica bibliotecaMusica) {
	deletarPreferencia(bibliotecaMusica.getUsuario(), bibliotecaMusica.getMusica());

	bibliotecaMusica.setGostei(0L);
	return bibliotecaMusicaDB.save(bibliotecaMusica);
  }

  private BibliotecaMusica deletarPreferencia(Usuario usuario, Musica musica) {
	return bibliotecaMusicaDB.deletarPreferencia(usuario, musica);
  }

  public List<Musica> carregaPreferenciais(List<Musica> listaMusicas, Usuario usuario) {
	for (Musica musica : listaMusicas) {
	  musica.setPreferencial(bibliotecaMusicaDB.isMusicaPreferencial(usuario, musica));
	}

	return listaMusicas;
  }

  public List<BibliotecaMusica> carregaBibliotecaMusicasUsuario(Usuario usuario) {
	return bibliotecaMusicaDB.carregaBibliotecaMusicasUsuario(usuario);
  }

}