package com.hoodwatcher.rn;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import com.hoodwatcher.ed.AbstractEntity;

public abstract class AbstractRN<T extends AbstractEntity, PK extends Number> {

  /**
   * Classe da entidade, necess�rio para o m�todo
   * <code>EntityManager.find</code>.
   */
  private Class<T> entityClass;

  public AbstractRN(Class<T> entityClass) {
	this.entityClass = entityClass;
  }

  /**
   * Exige a defini��o do <code>EntityManager</code> respons�vel pelas opera��es
   * de persist�ncia.
   */
  protected abstract EntityManager getEntityManager();

  @PostConstruct
  protected abstract void init();

}