package com.hoodwatcher.rn;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hoodwatcher.db.TimelineDB;
import com.hoodwatcher.ed.Timeline;
import com.hoodwatcher.ed.Usuario;

@Stateless
public class TimelineRN extends AbstractRN<Timeline, Long> {

  private TimelineDB timelineDB;

  @Inject
  MusicaRN musicaRN;

  @PersistenceContext(unitName = "HoodWatcherUnit")
  private EntityManager em;

  public TimelineRN() {
	super(Timeline.class);
  }

  @Override
  protected EntityManager getEntityManager() {
	return em;
  }

  @Override
  @PostConstruct
  public void init() {
	timelineDB = new TimelineDB(getEntityManager());
  }

  public Timeline carregaTimeline(Usuario usuario) {
	// Carrega timeline atual
	return consultar(1L);
  }

  public Timeline consultar(Long id) {
	return timelineDB.find(id);
  }

}