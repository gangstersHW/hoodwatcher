package com.hoodwatcher.rn;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hoodwatcher.db.MusicaDB;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Timeline;
import com.hoodwatcher.ed.Usuario;

@Stateless
public class MusicaRN extends AbstractRN<Musica, Long> {

  private MusicaDB musicaDB;
  
  @Inject
  BibliotecaMusicaRN bibliotecaMusicaRN;

  @PersistenceContext(unitName = "HoodWatcherUnit")
  private EntityManager em;

  public MusicaRN() {
	super(Musica.class);
  }

  @Override
  protected EntityManager getEntityManager() {
	return em;
  }

  @Override
  @PostConstruct
  public void init() {
	musicaDB = new MusicaDB(getEntityManager());
  }

  public List<Musica> consultarMusicasTimeline(Usuario usuario, Timeline timeline) {
	List<Musica> listaMusicas = musicaDB.findMusicasTimeline(usuario, timeline);
	listaMusicas = bibliotecaMusicaRN.carregaPreferenciais(listaMusicas, usuario);

	return listaMusicas;
  }
  
}