package com.hoodwatcher.rn;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hoodwatcher.db.UsuarioDB;
import com.hoodwatcher.ed.Usuario;

@Stateless
public class UsuarioRN extends AbstractRN<Usuario, Long> {

  private UsuarioDB usuarioDB;

  @PersistenceContext(unitName = "HoodWatcherUnit")
  private EntityManager em;

  public UsuarioRN() {
	super(Usuario.class);
  }

  @Override
  protected EntityManager getEntityManager() {
	return em;
  }

  @Override
  @PostConstruct
  public void init() {
	usuarioDB = new UsuarioDB(getEntityManager());
  }
  
  public Usuario carrega(Long id) {
	return usuarioDB.find(id);
  }

}