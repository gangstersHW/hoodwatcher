package com.hoodwatcher.ed;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the biblioteca_artista database table.
 * 
 */
@Entity
@Table(name = "biblioteca_artista")
@NamedQuery(name = "BibliotecaArtista.findAll", query = "SELECT b FROM BibliotecaArtista b")
public class BibliotecaArtista implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  // bi-directional many-to-one association to Usuario
  @ManyToOne(fetch = FetchType.EAGER)
  private Usuario usuario;

  // bi-directional many-to-one association to Artista
  @ManyToOne(fetch = FetchType.EAGER)
  private Artista artista;

  private Long gostei;

  public BibliotecaArtista() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Usuario getUsuario() {
	return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
  }

  public Artista getArtista() {
	return this.artista;
  }

  public void setArtista(Artista artista) {
	this.artista = artista;
  }

  public Long getGostei() {
	return gostei;
  }

  public void setGostei(Long gostei) {
	this.gostei = gostei;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((artista == null) ? 0 : artista.hashCode());
	result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	BibliotecaArtista other = (BibliotecaArtista) obj;
	if (artista == null) {
	  if (other.artista != null)
		return false;
	} else if (!artista.equals(other.artista))
	  return false;
	if (usuario == null) {
	  if (other.usuario != null)
		return false;
	} else if (!usuario.equals(other.usuario))
	  return false;
	return true;
  }

}