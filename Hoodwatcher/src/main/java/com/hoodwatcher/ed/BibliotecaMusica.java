package com.hoodwatcher.ed;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the biblioteca_musica database table.
 * 
 */
@Entity
@Table(name = "biblioteca_musica")
@NamedQueries({
	@NamedQuery(name = "BibliotecaMusica.findAll", query = "SELECT b FROM BibliotecaMusica b"),
	@NamedQuery(name = "BibliotecaMusica.deletarPreferencia", query = "DELETE FROM BibliotecaMusica b WHERE b.usuario = :usuario AND b.musica = :musica"),
	@NamedQuery(name = "BibliotecaMusica.verificaMusicaPreferencial", query = "SELECT bm FROM BibliotecaMusica bm WHERE bm.musica = :musica AND bm.usuario = :usuario AND bm.gostei = 1"),
	@NamedQuery(name = "BibliotecaMusica.carregaBibliotecaMusicasUsuario", query = "SELECT bm FROM BibliotecaMusica bm WHERE bm.usuario = :usuario AND bm.gostei = 1") })
public class BibliotecaMusica implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  public static String DELETAR_PREFERENCIA = "BibliotecaMusica.deletarPreferencia";
  public static String VERIFICA_MUSICA_PREFERENCIAL = "BibliotecaMusica.verificaMusicaPreferencial";
  public static String CARREGA_BIBLIOTECA_MUSICAS_USUARIO = "BibliotecaMusica.carregaBibliotecaMusicasUsuario";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  // bi-directional many-to-one association to Usuario
  @ManyToOne(fetch = FetchType.EAGER)
  private Usuario usuario;

  // bi-directional many-to-one association to Musica
  @ManyToOne(fetch = FetchType.EAGER)
  private Musica musica;

  private Long gostei;

  public BibliotecaMusica() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Usuario getUsuario() {
	return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
  }

  public Musica getMusica() {
	return this.musica;
  }

  public void setMusica(Musica musica) {
	this.musica = musica;
  }

  public Long getGostei() {
	return gostei;
  }

  public void setGostei(Long gostei) {
	this.gostei = gostei;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((musica == null) ? 0 : musica.hashCode());
	result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	BibliotecaMusica other = (BibliotecaMusica) obj;
	if (musica == null) {
	  if (other.musica != null)
		return false;
	} else if (!musica.equals(other.musica))
	  return false;
	if (usuario == null) {
	  if (other.usuario != null)
		return false;
	} else if (!usuario.equals(other.usuario))
	  return false;
	return true;
  }

}