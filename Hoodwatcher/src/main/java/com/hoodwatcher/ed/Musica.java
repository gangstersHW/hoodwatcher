package com.hoodwatcher.ed;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the musica database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Musica.findAll", query = "SELECT m FROM Musica m"),
// @NamedQuery(name = "Musica.findMusicasTimeline", query =
// "SELECT m FROM Musica m WHERE m.id NOT IN (SELECT bm.musica.id FROM BibliotecaMusica bm WHERE bm.like = 0 AND bm.usuario = :usuario) AND m.dtLancamento BETWEEN :dtInicial AND :dtFinal")})
	@NamedQuery(name = "Musica.findMusicasTimeline", query = "SELECT m FROM Musica m WHERE m.id NOT IN (SELECT bm.musica.id FROM BibliotecaMusica bm WHERE bm.gostei = 0 AND bm.usuario = :usuario)") })
public class Musica implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  public static String FIND_MUSICAS_TIMELINE = "Musica.findMusicasTimeline";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Temporal(TemporalType.DATE)
  @Column(name = "dt_lancamento")
  private Date dtLancamento;

  private Long faixa;

  private String nome;

  // bi-directional many-to-one association to BibliotecaMusica
  @OneToMany(mappedBy = "musica")
  private List<BibliotecaMusica> bibliotecaMusicas;

  // bi-directional many-to-one association to Album
  @ManyToOne(fetch = FetchType.EAGER)
  private Album album;

  // bi-directional many-to-one association to Artista
  @ManyToOne(fetch = FetchType.EAGER)
  private Artista artista;

  @Transient
  private boolean isPreferencial;

  public Musica() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Long getFaixa() {
	return faixa;
  }

  public void setFaixa(Long faixa) {
	this.faixa = faixa;
  }

  public Date getDtLancamento() {
	return this.dtLancamento;
  }

  public void setDtLancamento(Date dtLancamento) {
	this.dtLancamento = dtLancamento;
  }

  public String getNome() {
	return this.nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  public List<BibliotecaMusica> getBibliotecaMusicas() {
	return this.bibliotecaMusicas;
  }

  public void setBibliotecaMusicas(List<BibliotecaMusica> bibliotecaMusicas) {
	this.bibliotecaMusicas = bibliotecaMusicas;
  }

  public BibliotecaMusica addBibliotecaMusica(BibliotecaMusica bibliotecaMusica) {
	getBibliotecaMusicas().add(bibliotecaMusica);
	bibliotecaMusica.setMusica(this);

	return bibliotecaMusica;
  }

  public BibliotecaMusica removeBibliotecaMusica(BibliotecaMusica bibliotecaMusica) {
	getBibliotecaMusicas().remove(bibliotecaMusica);
	bibliotecaMusica.setMusica(null);

	return bibliotecaMusica;
  }

  public Album getAlbum() {
	return this.album;
  }

  public void setAlbum(Album album) {
	this.album = album;
  }

  public Artista getArtista() {
	return this.artista;
  }

  public void setArtista(Artista artista) {
	this.artista = artista;
  }

  public boolean isPreferencial() {
	return isPreferencial;
  }

  public void setPreferencial(boolean isPreferencial) {
	this.isPreferencial = isPreferencial;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Musica other = (Musica) obj;
	if (nome == null) {
	  if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	  return false;
	return true;
  }

}