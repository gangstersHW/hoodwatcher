package com.hoodwatcher.ed;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the album database table.
 * 
 */
@Entity
@NamedQuery(name = "Album.findAll", query = "SELECT a FROM Album a")
public class Album implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Temporal(TemporalType.DATE)
  @Column(name = "dt_lancamento")
  private Date dtLancamento;

  private String nome;

  // bi-directional many-to-one association to Artista
  @ManyToOne(fetch = FetchType.EAGER)
  private Artista artista;

  // bi-directional many-to-one association to BibliotecaAlbum
  @OneToMany(mappedBy = "album")
  private List<BibliotecaAlbum> bibliotecaAlbums;

  // bi-directional many-to-one association to Musica
  @OneToMany(mappedBy = "album")
  private List<Musica> musicas;

  public Album() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Date getDtLancamento() {
	return this.dtLancamento;
  }

  public void setDtLancamento(Date dtLancamento) {
	this.dtLancamento = dtLancamento;
  }

  public String getNome() {
	return this.nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  public Artista getArtista() {
	return this.artista;
  }

  public void setArtista(Artista artista) {
	this.artista = artista;
  }

  public List<BibliotecaAlbum> getBibliotecaAlbums() {
	return this.bibliotecaAlbums;
  }

  public void setBibliotecaAlbums(List<BibliotecaAlbum> bibliotecaAlbums) {
	this.bibliotecaAlbums = bibliotecaAlbums;
  }

  public BibliotecaAlbum addBibliotecaAlbum(BibliotecaAlbum bibliotecaAlbum) {
	getBibliotecaAlbums().add(bibliotecaAlbum);
	bibliotecaAlbum.setAlbum(this);

	return bibliotecaAlbum;
  }

  public BibliotecaAlbum removeBibliotecaAlbum(BibliotecaAlbum bibliotecaAlbum) {
	getBibliotecaAlbums().remove(bibliotecaAlbum);
	bibliotecaAlbum.setAlbum(null);

	return bibliotecaAlbum;
  }

  public List<Musica> getMusicas() {
	return this.musicas;
  }

  public void setMusicas(List<Musica> musicas) {
	this.musicas = musicas;
  }

  public Musica addMusica(Musica musica) {
	getMusicas().add(musica);
	musica.setAlbum(this);

	return musica;
  }

  public Musica removeMusica(Musica musica) {
	getMusicas().remove(musica);
	musica.setAlbum(null);

	return musica;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Album other = (Album) obj;
	if (nome == null) {
	  if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	  return false;
	return true;
  }

}