package com.hoodwatcher.ed;

import java.io.Serializable;

/**
 * Estipula um contrato base para as entidades persistentes da aplica��o. Esse
 * contrato � utilizado pelo componente respons�vel pelas opera��ess b�sicas de
 * persist�ncia: <code>AbstractPersistence</code>.
 * 
 * @author avalos
 * 
 */
public interface AbstractEntity extends Serializable {

  /**
   * @return A refer�ncia para a chave prim�ria (Primary Key) de cada objeto
   *         persistido. Caso o objeto ainda n�o tenha sido persistido, deve
   *         retornar <code>null</code>.
   */
  public Long getId();

}