package com.hoodwatcher.ed;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the biblioteca_album database table.
 * 
 */
@Entity
@Table(name = "biblioteca_album")
@NamedQuery(name = "BibliotecaAlbum.findAll", query = "SELECT b FROM BibliotecaAlbum b")
public class BibliotecaAlbum implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  // bi-directional many-to-one association to Usuario
  @ManyToOne(fetch = FetchType.EAGER)
  private Usuario usuario;

  // bi-directional many-to-one association to Album
  @ManyToOne(fetch = FetchType.EAGER)
  private Album album;

  private Long gostei;

  public BibliotecaAlbum() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Usuario getUsuario() {
	return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
  }

  public Album getAlbum() {
	return this.album;
  }

  public void setAlbum(Album album) {
	this.album = album;
  }

  public Long getGostei() {
	return gostei;
  }

  public void setGostei(Long gostei) {
	this.gostei = gostei;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((album == null) ? 0 : album.hashCode());
	result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	BibliotecaAlbum other = (BibliotecaAlbum) obj;
	if (album == null) {
	  if (other.album != null)
		return false;
	} else if (!album.equals(other.album))
	  return false;
	if (usuario == null) {
	  if (other.usuario != null)
		return false;
	} else if (!usuario.equals(other.usuario))
	  return false;
	return true;
  }

}