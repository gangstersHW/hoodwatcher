package com.hoodwatcher.ed;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * The persistent class for the artista database table.
 * 
 */
@Entity
@NamedQuery(name = "Artista.findAll", query = "SELECT a FROM Artista a")
public class Artista implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String nome;

  // bi-directional many-to-one association to Album
  @OneToMany(mappedBy = "artista")
  private List<Album> albums;

  // bi-directional many-to-one association to BibliotecaArtista
  @OneToMany(mappedBy = "artista")
  private List<BibliotecaArtista> bibliotecaArtistas;

  // bi-directional many-to-one association to Musica
  @OneToMany(mappedBy = "artista")
  private List<Musica> musicas;

  public Artista() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public String getNome() {
	return this.nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  public List<Album> getAlbums() {
	return this.albums;
  }

  public void setAlbums(List<Album> albums) {
	this.albums = albums;
  }

  public Album addAlbum(Album album) {
	getAlbums().add(album);
	album.setArtista(this);

	return album;
  }

  public Album removeAlbum(Album album) {
	getAlbums().remove(album);
	album.setArtista(null);

	return album;
  }

  public List<BibliotecaArtista> getBibliotecaArtistas() {
	return this.bibliotecaArtistas;
  }

  public void setBibliotecaArtistas(List<BibliotecaArtista> bibliotecaArtistas) {
	this.bibliotecaArtistas = bibliotecaArtistas;
  }

  public BibliotecaArtista addBibliotecaArtista(BibliotecaArtista bibliotecaArtista) {
	getBibliotecaArtistas().add(bibliotecaArtista);
	bibliotecaArtista.setArtista(this);

	return bibliotecaArtista;
  }

  public BibliotecaArtista removeBibliotecaArtista(BibliotecaArtista bibliotecaArtista) {
	getBibliotecaArtistas().remove(bibliotecaArtista);
	bibliotecaArtista.setArtista(null);

	return bibliotecaArtista;
  }

  public List<Musica> getMusicas() {
	return this.musicas;
  }

  public void setMusicas(List<Musica> musicas) {
	this.musicas = musicas;
  }

  public Musica addMusica(Musica musica) {
	getMusicas().add(musica);
	musica.setArtista(this);

	return musica;
  }

  public Musica removeMusica(Musica musica) {
	getMusicas().remove(musica);
	musica.setArtista(null);

	return musica;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Artista other = (Artista) obj;
	if (nome == null) {
	  if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	  return false;
	return true;
  }

}