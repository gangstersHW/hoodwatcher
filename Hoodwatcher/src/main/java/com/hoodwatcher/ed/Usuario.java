package com.hoodwatcher.ed;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String email;

  private String nome;

  private String senha;

  // bi-directional many-to-one association to BibliotecaAlbum
  @OneToMany(mappedBy = "usuario")
  private List<BibliotecaAlbum> bibliotecaAlbums;

  // bi-directional many-to-one association to BibliotecaArtista
  @OneToMany(mappedBy = "usuario")
  private List<BibliotecaArtista> bibliotecaArtistas;

  // bi-directional many-to-one association to BibliotecaMusica
  @OneToMany(mappedBy = "usuario")
  private List<BibliotecaMusica> bibliotecaMusicas;

  @Transient
  private boolean logado;

  public Usuario() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public String getEmail() {
	return this.email;
  }

  public void setEmail(String email) {
	this.email = email;
  }

  public String getNome() {
	return this.nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  public String getSenha() {
	return this.senha;
  }

  public void setSenha(String senha) {
	this.senha = senha;
  }

  public List<BibliotecaAlbum> getBibliotecaAlbums() {
	if (this.bibliotecaAlbums == null) {
	  this.bibliotecaAlbums = new ArrayList<BibliotecaAlbum>();
	}
	return this.bibliotecaAlbums;
  }

  public void setBibliotecaAlbums(List<BibliotecaAlbum> bibliotecaAlbums) {
	this.bibliotecaAlbums = bibliotecaAlbums;
  }

  public BibliotecaAlbum addBibliotecaAlbum(BibliotecaAlbum bibliotecaAlbum) {
	getBibliotecaAlbums().add(bibliotecaAlbum);
	bibliotecaAlbum.setUsuario(this);

	return bibliotecaAlbum;
  }

  public BibliotecaAlbum removeBibliotecaAlbum(BibliotecaAlbum bibliotecaAlbum) {
	getBibliotecaAlbums().remove(bibliotecaAlbum);
	bibliotecaAlbum.setUsuario(null);

	return bibliotecaAlbum;
  }

  public List<BibliotecaArtista> getBibliotecaArtistas() {
	if (this.bibliotecaArtistas == null) {
	  this.bibliotecaArtistas = new ArrayList<BibliotecaArtista>();
	}
	return this.bibliotecaArtistas;
  }

  public void setBibliotecaArtistas(List<BibliotecaArtista> bibliotecaArtistas) {
	this.bibliotecaArtistas = bibliotecaArtistas;
  }

  public BibliotecaArtista addBibliotecaArtista(BibliotecaArtista bibliotecaArtista) {
	getBibliotecaArtistas().add(bibliotecaArtista);
	bibliotecaArtista.setUsuario(this);

	return bibliotecaArtista;
  }

  public BibliotecaArtista removeBibliotecaArtista(BibliotecaArtista bibliotecaArtista) {
	getBibliotecaArtistas().remove(bibliotecaArtista);
	bibliotecaArtista.setUsuario(null);

	return bibliotecaArtista;
  }

  public List<BibliotecaMusica> getBibliotecaMusicas() {
	if (this.bibliotecaMusicas == null) {
	  this.bibliotecaMusicas = new ArrayList<BibliotecaMusica>();
	}
	return this.bibliotecaMusicas;
  }

  public void setBibliotecaMusicas(List<BibliotecaMusica> bibliotecaMusicas) {
	this.bibliotecaMusicas = bibliotecaMusicas;
  }

  public BibliotecaMusica addBibliotecaMusica(BibliotecaMusica bibliotecaMusica) {
	getBibliotecaMusicas().add(bibliotecaMusica);
	bibliotecaMusica.setUsuario(this);

	return bibliotecaMusica;
  }

  public BibliotecaMusica removeBibliotecaMusica(BibliotecaMusica bibliotecaMusica) {
	getBibliotecaMusicas().remove(bibliotecaMusica);
	bibliotecaMusica.setUsuario(null);

	return bibliotecaMusica;
  }

  public boolean isLogado() {
	return logado;
  }

  public void setLogado(boolean logado) {
	this.logado = logado;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Usuario other = (Usuario) obj;
	if (nome == null) {
	  if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	  return false;
	return true;
  }

}