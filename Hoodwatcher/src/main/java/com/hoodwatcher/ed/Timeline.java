package com.hoodwatcher.ed;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the timeline database table.
 * 
 */
@Entity
@NamedQuery(name = "Timeline.findAll", query = "SELECT t FROM Timeline t")
public class Timeline implements AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Temporal(TemporalType.DATE)
  @Column(name = "dt_final")
  private Date dtFinal;

  @Temporal(TemporalType.DATE)
  @Column(name = "dt_inicial")
  private Date dtInicial;

  private String nome;

  public Timeline() {
  }

  public Long getId() {
	return this.id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public Date getDtFinal() {
	return this.dtFinal;
  }

  public void setDtFinal(Date dtFinal) {
	this.dtFinal = dtFinal;
  }

  public Date getDtInicial() {
	return this.dtInicial;
  }

  public void setDtInicial(Date dtInicial) {
	this.dtInicial = dtInicial;
  }

  public String getNome() {
	return this.nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Timeline other = (Timeline) obj;
	if (nome == null) {
	  if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	  return false;
	return true;
  }

}