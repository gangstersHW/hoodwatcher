package com.hoodwatcher.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.hoodwatcher.ed.BibliotecaMusica;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Usuario;

public class BibliotecaMusicaDB extends AbstractDB<BibliotecaMusica, Long> {

  public BibliotecaMusicaDB(EntityManager em) {
	super(em, BibliotecaMusica.class);
  }

  public BibliotecaMusica deletarPreferencia(Usuario usuario, Musica musica) {
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put("usuario", usuario);
	parameters.put("musica", musica);

	return super.removeFromQuery(BibliotecaMusica.DELETAR_PREFERENCIA, parameters);
  }

  public boolean isMusicaPreferencial(Usuario usuario, Musica musica) {
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put("usuario", usuario);
	parameters.put("musica", musica);

	BibliotecaMusica bibliotecaMusica = super.findOneResult(BibliotecaMusica.VERIFICA_MUSICA_PREFERENCIAL, parameters);

	if (bibliotecaMusica != null) {
	  return true;
	}
	return false;
  }

  public List<BibliotecaMusica> carregaBibliotecaMusicasUsuario(Usuario usuario) {
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put("usuario", usuario);

	return super.findMultiplesResult(BibliotecaMusica.CARREGA_BIBLIOTECA_MUSICAS_USUARIO, parameters);
  }

}