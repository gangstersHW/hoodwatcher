package com.hoodwatcher.db;

import javax.persistence.EntityManager;

import com.hoodwatcher.ed.Usuario;

public class UsuarioDB extends AbstractDB<Usuario, Long> {

  public UsuarioDB(EntityManager em) {
	super(em, Usuario.class);
  }

}