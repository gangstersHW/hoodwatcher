package com.hoodwatcher.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Timeline;
import com.hoodwatcher.ed.Usuario;

public class MusicaDB extends AbstractDB<Musica, Long> {

  public MusicaDB(EntityManager em) {
	super(em, Musica.class);
  }

  public List<Musica> findMusicasTimeline(Usuario usuario, Timeline timeline) {
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put("usuario", usuario);
	// parameters.put("dtInicial", timeline.getDtInicial());
	// parameters.put("dtFinal", timeline.getDtFinal());

	return super.findMultiplesResult(Musica.FIND_MUSICAS_TIMELINE, parameters);
  }

}