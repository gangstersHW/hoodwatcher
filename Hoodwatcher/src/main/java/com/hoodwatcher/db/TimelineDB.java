package com.hoodwatcher.db;

import javax.persistence.EntityManager;

import com.hoodwatcher.ed.Timeline;

public class TimelineDB extends AbstractDB<Timeline, Long> {

  public TimelineDB(EntityManager em) {
	super(em, Timeline.class);
  }

}