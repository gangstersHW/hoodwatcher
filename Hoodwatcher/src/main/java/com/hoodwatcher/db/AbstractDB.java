package com.hoodwatcher.db;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.hoodwatcher.ed.AbstractEntity;

public abstract class AbstractDB<T extends AbstractEntity, PK extends Number> {

  private EntityManager em;

  private Class<T> entityClass;

  public AbstractDB(EntityManager em, Class<T> entityClass) {
	this.em = em;
	this.entityClass = entityClass;
  }

  public T save(T entityClass) {
	if (entityClass.getId() != null) {
	  return getEntityManager().merge(entityClass);
	} else {
	  getEntityManager().persist(entityClass);
	  return entityClass;
	}
  }

  public void remove(T entityClass) {
	getEntityManager().remove(getEntityManager().merge(entityClass));
  }

  public T find(PK id) {
	return getEntityManager().find(entityClass, id);
  }

  public List<T> findAll() {
	CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
	cq.select(cq.from(entityClass));
	return getEntityManager().createQuery(cq).getResultList();
  }

  public List<T> findRange(int[] range) {
	CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
	cq.select(cq.from(entityClass));
	Query q = getEntityManager().createQuery(cq);
	q.setMaxResults(range[1] - range[0]);
	q.setFirstResult(range[0]);
	return q.getResultList();
  }

  public int count() {
	CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
	Root<T> rt = cq.from(entityClass);
	cq.select(getEntityManager().getCriteriaBuilder().count(rt));
	Query q = getEntityManager().createQuery(cq);
	return ((Long) q.getSingleResult()).intValue();
  }

  public T findOneResult(String namedQuery, Map<String, Object> parameters) {
	T result = null;
	try {
	  Query query = em.createNamedQuery(namedQuery);
	  if (parameters != null && !parameters.isEmpty()) {
		populateQueryParameters(query, parameters);
	  }
	  result = (T) query.getSingleResult();
	} catch (NoResultException e) {
	  System.out.println("No result found for named query: " + namedQuery);
	} catch (Exception e) {
	  System.out.println("Error while running query: " + e.getMessage());
	  e.printStackTrace();
	}
	return result;
  }

  public T removeFromQuery(String namedQuery, Map<String, Object> parameters) {
	T result = null;
	try {
	  Query query = em.createNamedQuery(namedQuery);
	  if (parameters != null && !parameters.isEmpty()) {
		populateQueryParameters(query, parameters);
	  }
	  query.executeUpdate();
	} catch (Exception e) {
	  System.out.println("Error while running query: " + e.getMessage());
	  e.printStackTrace();
	}
	return result;
  }

  public List<T> findMultiplesResult(String namedQuery, Map<String, Object> parameters) {
	List<T> result = null;
	try {
	  Query query = em.createNamedQuery(namedQuery);
	  if (parameters != null && !parameters.isEmpty()) {
		populateQueryParameters(query, parameters);
	  }
	  result = query.getResultList();
	} catch (NoResultException e) {
	  System.out.println("No result found for named query: " + namedQuery);
	} catch (Exception e) {
	  System.out.println("Error while running query: " + e.getMessage());
	  e.printStackTrace();
	}
	return result;
  }

  private void populateQueryParameters(Query query, Map<String, Object> parameters) {
	for (Entry<String, Object> entry : parameters.entrySet()) {
	  query.setParameter(entry.getKey(), entry.getValue());
	}
  }

  private EntityManager getEntityManager() {
	return em;
  }

}