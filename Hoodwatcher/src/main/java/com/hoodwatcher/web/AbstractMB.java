package com.hoodwatcher.web;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.Serializable;
import java.util.ResourceBundle;

import org.primefaces.context.RequestContext;

import com.hoodwatcher.infra.JSFMessageUtil;

/**
 * Estipula um contrato base para os manage beans da aplica��o.
 * 
 * @author avalos
 * 
 */
public abstract class AbstractMB implements Serializable {

  private static final String KEEP_DIALOG_OPENED = "KEEP_DIALOG_OPENED";

  public AbstractMB() {
	super();
  }

  protected void displayErrorMessageToUser(String message) {
	JSFMessageUtil messageUtil = new JSFMessageUtil();
	messageUtil.sendErrorMessageToUser(message);
  }

  protected void displayInfoMessageToUser(String message) {
	JSFMessageUtil messageUtil = new JSFMessageUtil();
	messageUtil.sendInfoMessageToUser(message);
  }

  protected void closeDialog() {
	getRequestContext().addCallbackParam(KEEP_DIALOG_OPENED, false);
  }

  protected void keepDialogOpen() {
	getRequestContext().addCallbackParam(KEEP_DIALOG_OPENED, true);
  }

  protected RequestContext getRequestContext() {
	return RequestContext.getCurrentInstance();
  }

  protected String getMessageFromI18N(String key) {
	ResourceBundle bundle = ResourceBundle.getBundle("messages", getCurrentInstance().getViewRoot().getLocale());
	return bundle.getString(key);
  }

}