package com.hoodwatcher.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.hoodwatcher.ed.BibliotecaMusica;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Timeline;
import com.hoodwatcher.ed.Usuario;
import com.hoodwatcher.rn.BibliotecaMusicaRN;
import com.hoodwatcher.rn.MusicaRN;
import com.hoodwatcher.rn.TimelineRN;
import com.hoodwatcher.rn.UsuarioRN;

/**
 * 
 * @author avalos
 * 
 */
@Named
@ViewScoped
public class TimelineMB extends AbstractMB {

  private static final long serialVersionUID = 1L;

  @Inject
  TimelineRN timelineRN;

  @Inject
  MusicaRN musicaRN;

  @Inject
  UsuarioRN usuarioRN;

  @Inject
  BibliotecaMusicaRN bibliotecaMusicaRN;

  private Usuario usuario;

  private Timeline timeline;

  private List<Musica> listaMusicas;

  private Musica musicaSelecionada;

  @PostConstruct
  void init() {
	carregaUsuario();
	carregaTimeline();
	carregaMusicas();
  }

  public void actionLike() {
	try {
	  BibliotecaMusica bibliotecaMusica = new BibliotecaMusica();
	  bibliotecaMusica.setMusica(musicaSelecionada);
	  bibliotecaMusica.setUsuario(usuario);

	  bibliotecaMusicaRN.salvarLike(bibliotecaMusica);

	  displayInfoMessageToUser("M�sica adicionada � biblioteca de prefer�ncias!");
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao salvar registro!");
	}
  }

  public void actionNoLike() {
	try {
	  BibliotecaMusica bibliotecaMusica = new BibliotecaMusica();
	  bibliotecaMusica.setMusica(musicaSelecionada);
	  bibliotecaMusica.setUsuario(usuario);

	  bibliotecaMusicaRN.salvarNoLike(bibliotecaMusica);
	  displayInfoMessageToUser("M�sica retirada da timeline!");
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao salvar registro!");
	}
  }

  private void carregaUsuario() {
	try {
	  usuario = usuarioRN.carrega(1L);
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao carregar usuario!");
	}
  }

  private void carregaTimeline() {
	try {
	  timeline = timelineRN.carregaTimeline(usuario);
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao carregar timeline!");
	}
  }

  private void carregaMusicas() {
	try {
	  listaMusicas = musicaRN.consultarMusicasTimeline(usuario, timeline);
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao carregar m�sicas!");
	}
  }

  public Usuario getUsuario() {
	return usuario;
  }

  public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
  }

  public Timeline getTimeline() {
	return timeline;
  }

  public List<Musica> getListaMusicas() {
	carregaMusicas();
	return listaMusicas;
  }

  public void setListaMusicas(List<Musica> listaMusicas) {
	this.listaMusicas = listaMusicas;
  }

  public void setTimeline(Timeline timeline) {
	this.timeline = timeline;
  }

  public Musica getMusicaSelecionada() {
	return musicaSelecionada;
  }

  public void setMusicaSelecionada(Musica musicaSelecionada) {
	this.musicaSelecionada = musicaSelecionada;
  }

}