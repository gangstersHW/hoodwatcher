package com.hoodwatcher.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.hoodwatcher.ed.BibliotecaMusica;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Usuario;
import com.hoodwatcher.rn.BibliotecaMusicaRN;
import com.hoodwatcher.rn.UsuarioRN;

/**
 * 
 * @author avalos
 * 
 */
@Named
@ViewScoped
public class BibliotecaMusicaMB extends AbstractMB {

  private static final long serialVersionUID = 1L;

  @Inject
  BibliotecaMusicaRN BibliotecaMusicaRN;

  @Inject
  UsuarioRN usuarioRN;

  @Inject
  BibliotecaMusicaRN bibliotecaMusicaRN;

  private Usuario usuario;

  private List<BibliotecaMusica> listaBibliotecaMusicas;

  private Musica musicaSelecionada;

  @PostConstruct
  void init() {
	carregaUsuario();
	carregaBibliotecaMusicas();
  }

  private void carregaUsuario() {
	try {
	  usuario = usuarioRN.carrega(1L);
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao carregar usuario!");
	}
  }

  private void carregaBibliotecaMusicas() {
	try {
	  listaBibliotecaMusicas = bibliotecaMusicaRN.carregaBibliotecaMusicasUsuario(usuario);
	} catch (Exception e) {
	  displayErrorMessageToUser("Erro ao carregar biblioteca de m�sicas!");
	}
  }

  public Usuario getUsuario() {
	return usuario;
  }

  public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
  }

  public List<BibliotecaMusica> getListaBibliotecaMusicas() {
	return listaBibliotecaMusicas;
  }

  public void setListaBibliotecaMusicas(List<BibliotecaMusica> listaBibliotecaMusicas) {
	this.listaBibliotecaMusicas = listaBibliotecaMusicas;
  }

  public Musica getMusicaSelecionada() {
	return musicaSelecionada;
  }

  public void setMusicaSelecionada(Musica musicaSelecionada) {
	this.musicaSelecionada = musicaSelecionada;
  }

}