package com.hoodwatcher;

import java.util.List;

import com.hoodwatcher.ed.Album;
import com.hoodwatcher.ed.Artista;
import com.hoodwatcher.ed.BibliotecaAlbum;
import com.hoodwatcher.ed.BibliotecaArtista;
import com.hoodwatcher.ed.BibliotecaMusica;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Usuario;

public class HoodWatcher {

  private static Usuario usuarioLogado;

  private static String mensagem;

  private static Musica sugestaoMusica;

  private static List<Musica> listaMusicas;

  private static List<Album> listaAlbuns;

  public static void addArtistaNaBiblioteca(Artista artista) {
	BibliotecaArtista bibliotecaArtista = new BibliotecaArtista();
	bibliotecaArtista.setUsuario(usuarioLogado);
	bibliotecaArtista.setArtista(artista);

	usuarioLogado.getBibliotecaArtistas().add(bibliotecaArtista);
  }

  public static void delArtistaNaBiblioteca(Artista artista) {
	BibliotecaArtista bibliotecaArtista = new BibliotecaArtista();
	bibliotecaArtista.setUsuario(usuarioLogado);
	bibliotecaArtista.setArtista(artista);

	usuarioLogado.getBibliotecaArtistas().remove(bibliotecaArtista);
  }

  public static boolean temArtistaNaBiblioteca(Artista artista) {
	BibliotecaArtista bibliotecaArtista = new BibliotecaArtista();
	bibliotecaArtista.setUsuario(usuarioLogado);
	bibliotecaArtista.setArtista(artista);

	return usuarioLogado.getBibliotecaArtistas().contains(bibliotecaArtista);
  }

  public static void addAlbumNaBiblioteca(Album album) {
	if (temAlbumNaBiblioteca(album)) {
	  mensagem = "�lbum previamente adicionado na biblioteca.";
	} else {
	  BibliotecaAlbum bibliotecaAlbum = new BibliotecaAlbum();
	  bibliotecaAlbum.setUsuario(usuarioLogado);
	  bibliotecaAlbum.setAlbum(album);

	  usuarioLogado.getBibliotecaAlbums().add(bibliotecaAlbum);
	}
  }

  public static void delAlbumNaBiblioteca(Album album) {
	BibliotecaAlbum bibliotecaAlbum = new BibliotecaAlbum();
	bibliotecaAlbum.setUsuario(usuarioLogado);
	bibliotecaAlbum.setAlbum(album);

	usuarioLogado.getBibliotecaAlbums().remove(bibliotecaAlbum);
  }

  public static boolean temAlbumNaBiblioteca(Album album) {
	BibliotecaAlbum bibliotecaAlbum = new BibliotecaAlbum();
	bibliotecaAlbum.setUsuario(usuarioLogado);
	bibliotecaAlbum.setAlbum(album);

	return usuarioLogado.getBibliotecaAlbums().contains(bibliotecaAlbum);
  }

  public static void addMusicaNaBiblioteca(Musica musica) {
	BibliotecaMusica bibliotecaMusica = new BibliotecaMusica();
	bibliotecaMusica.setUsuario(usuarioLogado);
	bibliotecaMusica.setMusica(musica);

	usuarioLogado.getBibliotecaMusicas().add(bibliotecaMusica);
  }

  public static void delMusicaNaBiblioteca(Musica musica) {
	BibliotecaMusica bibliotecaMusica = new BibliotecaMusica();
	bibliotecaMusica.setUsuario(usuarioLogado);
	bibliotecaMusica.setMusica(musica);

	usuarioLogado.getBibliotecaMusicas().remove(bibliotecaMusica);
  }

  public static boolean temMusicaNaBiblioteca(Musica musica) {
	BibliotecaMusica bibliotecaMusica = new BibliotecaMusica();
	bibliotecaMusica.setUsuario(usuarioLogado);
	bibliotecaMusica.setMusica(musica);	

	return usuarioLogado.getBibliotecaMusicas().contains(bibliotecaMusica);
  }

  public static void setUsuarioLogado(Usuario usuario) {
	usuarioLogado = usuario;
  }

  public static Usuario getUsuarioLogado() {
	return usuarioLogado;
  }

  public static String getMensagem() {
	return mensagem;
  }

  public static void setMensagem(String mensagem) {
	HoodWatcher.mensagem = mensagem;
  }

  public static Musica getSugestaoMusica() {
	return sugestaoMusica;
  }

  public static void setSugestaoMusica(Musica sugestaoMusica) {
	HoodWatcher.sugestaoMusica = sugestaoMusica;
  }

  public static List<Musica> getListaMusicas() {
	return listaMusicas;
  }

  public static void setListaMusicas(List<Musica> listaMusicas) {
	HoodWatcher.listaMusicas = listaMusicas;
  }

  public static List<Album> getListaAlbuns() {
	return listaAlbuns;
  }

  public static void setListaAlbuns(List<Album> listaAlbuns) {
	HoodWatcher.listaAlbuns = listaAlbuns;
  }

}