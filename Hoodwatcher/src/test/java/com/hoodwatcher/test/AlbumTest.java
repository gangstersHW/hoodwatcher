package com.hoodwatcher.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.hoodwatcher.HoodWatcher;
import com.hoodwatcher.ed.Album;
import com.hoodwatcher.ed.Artista;
import com.hoodwatcher.ed.Musica;
import com.hoodwatcher.ed.Usuario;

public class AlbumTest {

  @Before
  public void setUp() {
	Usuario usuario = new Usuario();
	usuario.setNome("Felipe");
	usuario.setLogado(true);

	HoodWatcher.setUsuarioLogado(usuario);
  }

  /*
   * Hist�ria 1
   * 
   * Cen�rio: Novo �lbum lan�ado.
   * 
   * Dado que o �lbum "Swish" foi lan�ado e "Felipe" est� logado no sistema
   * Quando acessar o website ent�o o sistema deve mostrar em uma linha de tempo
   * lan�amentos da �ltima semana e permitir visualizar informa��es sobre o
   * �lbum "Swish".
   */
  @Test
  public void testAlbumLancamento() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	HoodWatcher.getListaAlbuns();
  }

  /*
   * Hist�ria 2
   * 
   * Cen�rio: Novo single lan�ado.
   * 
   * Dado que um single "Bitch don't kill my vibe" foi lan�ado e "Felipe" est�
   * logado no sistema quando acessar o website ent�o o sistema deve mostrar em
   * uma linha de tempo lan�amentos da �ltima semana e permitir visualizar
   * informa��es sobre o single "Bitch don't kill my vibe".
   */
  @Test
  public void testSingleLancamento() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	HoodWatcher.getListaMusicas();
  }

  /*
   * Hist�ria 3
   * 
   * Cen�rio: Adicionando artista a biblioteca.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer adicionar o artista
   * "Kane West" quando ele inserir o nome desse artista ent�o o sistema deve
   * associar esse artista a sua biblioteca.
   */
  @Test
  public void testAdicionaArtistaBiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Artista artista = new Artista();
	artista.setNome("Kanye West");

	HoodWatcher.addArtistaNaBiblioteca(artista);
	assertEquals(true, HoodWatcher.temArtistaNaBiblioteca(artista));

  }

  /*
   * Historia 4
   * 
   * Cen�rio: Adicionando �lbum novo a biblioteca.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer adicionar o �lbum
   * "School Dropout" quando ele inserir o nome desse �lbum ent�o o sistema deve
   * associar esse �lbum a sua biblioteca.
   */
  @Test
  public void testAdicionaAlbumABiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Album album = new Album();
	album.setNome("School Dropout");

	HoodWatcher.addAlbumNaBiblioteca(album);
	assertEquals(true, HoodWatcher.temAlbumNaBiblioteca(album));
  }

  /*
   * Hist�ria 4
   * 
   * Cen�rio: �lbum j� existente na bilbioteca.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer adicionar o �lbum
   * "School Dropout" quando ele inserir o nome desse �lbum e esse �lbum j�
   * existir em sua biblioteca ent�o o sistema deve avis�-lo que o �lbum j� se
   * encontra adicionado.
   */
  @Test
  public void testAlbumJaExisteNaBiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Album album = new Album();
	album.setNome("School Dropout");

	HoodWatcher.addAlbumNaBiblioteca(album);
	assertEquals(true, HoodWatcher.temAlbumNaBiblioteca(album));

	HoodWatcher.addAlbumNaBiblioteca(album);

	assertEquals("�lbum previamente adicionado na biblioteca.", HoodWatcher.getMensagem());
  }

  /*
   * Historia 4
   * 
   * Cen�rio: Adicionando um novo single � biblioteca.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer adicionar o single
   * "Bitch don't kill my vibe" quando ele inserir o nome desse single e esse
   * �lbum j� existir em sua biblioteca ent�o o sistema deve associar esse
   * single a sua biblioteca.
   */

  @Test
  public void testAdicionaSingleABiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Musica musica = new Musica();
	musica.setNome("Bitch don't kill my vibe");

	HoodWatcher.addMusicaNaBiblioteca(musica);
	;

	assertEquals(true, HoodWatcher.temMusicaNaBiblioteca(musica));
  }

  /*
   * Hist�ria 5
   * 
   * Cen�rio: Removendo uma single da minha lista de prefer�ncias.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer remover o single
   * "Bitch don't kill my vibe" quando ele inserir o nome desse single e esse
   * �lbum j� existir em sua biblioteca ent�o o sistema deve desassociar esse
   * single da sua biblioteca.
   */
  @Test
  public void testRemoveSingleDaBiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Musica musica = new Musica();
	musica.setNome("Bitch don't kill my vibe");

	HoodWatcher.addMusicaNaBiblioteca(musica);
	assertEquals(true, HoodWatcher.temMusicaNaBiblioteca(musica));

	HoodWatcher.delMusicaNaBiblioteca(musica);
	assertEquals(false, HoodWatcher.temMusicaNaBiblioteca(musica));
  }

  /*
   * Hist�ria 5
   * 
   * Cen�rio: Removendo um �lbum da minha lista de prefer�ncias.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer remover o �lbum
   * "School Dropout" quando ele inserir o nome desse �lbum e esse �lbum j�
   * existir em sua biblioteca ent�o o sistema deve desassociar esse �lbum da
   * sua biblioteca.
   */
  @Test
  public void testRemoveAlbumDaBiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Album album = new Album();
	album.setNome("School Dropout");

	HoodWatcher.addAlbumNaBiblioteca(album);
	assertEquals(true, HoodWatcher.temAlbumNaBiblioteca(album));

	HoodWatcher.delAlbumNaBiblioteca(album);
	assertEquals(false, HoodWatcher.temAlbumNaBiblioteca(album));
  }

  /*
   * Hist�ria 5
   * 
   * Cen�rio: Removendo um artista da minha lista de prefer�ncias.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele quer remover o artista
   * "Kanye West" quando ele inserir o nome desse artista e esse artista j�
   * existir em sua biblioteca ent�o o sistema deve desassociar esse �lbum da
   * sua biblioteca.
   */
  @Test
  public void testRemoveArtistaDaBiblioteca() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Artista artista = new Artista();
	artista.setNome("Kanye West");

	HoodWatcher.addArtistaNaBiblioteca(artista);
	assertEquals(true, HoodWatcher.temArtistaNaBiblioteca(artista));

	HoodWatcher.delArtistaNaBiblioteca(artista);
	assertEquals(false, HoodWatcher.temArtistaNaBiblioteca(artista));
  }

  /*
   * Hist�ria 6
   * 
   * Cen�rio: Mostrar sugest�o baseado nas pref�ncias.
   * 
   * Dado que o usu�rio "Felipe" est� logado e que ele j� adicionou "Kanye West"
   * � sua lista de prefer�ncias, quando ele acessar sua timeline ent�o o
   * sistema deve sugerir que ele ou�a o artista Drake.
   */
  @Test
  public void testMostraSugestao() {
	assertEquals(true, HoodWatcher.getUsuarioLogado().isLogado());
	assertEquals("Felipe", HoodWatcher.getUsuarioLogado().getNome());

	Artista artista = new Artista();
	artista.setNome("Kanye West");

	HoodWatcher.addArtistaNaBiblioteca(artista);
	assertEquals(true, HoodWatcher.temArtistaNaBiblioteca(artista));

	Album album = new Album();
	album.setNome("School Dropout");

	HoodWatcher.addAlbumNaBiblioteca(album);
	assertEquals(true, HoodWatcher.temAlbumNaBiblioteca(album));

	Musica musica = new Musica();
	musica.setNome("Drake");
	
	HoodWatcher.setSugestaoMusica(musica);
  }

}